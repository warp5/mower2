package fr.warp5.mower2;

import java.util.List;

import fr.warp5.mower2.core.Motion;
import fr.warp5.mower2.core.Mower;
import fr.warp5.mower2.core.Status;
import fr.warp5.mower2.engine.IMotionEngine;

public class Simulation {
    
    private Mower mower;
    private Scenario scenario;
    
    public Simulation(IMotionEngine engine, Scenario scenario) {
        super();
        this.mower =  new Mower(scenario.getInitialStatus(),engine);
        this.scenario = scenario;
    }

    public Status run(){
        List<Motion> motions = scenario.getCommands();
        return mower.move(motions);
    }
    

}
