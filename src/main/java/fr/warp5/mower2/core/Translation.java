package fr.warp5.mower2.core;

public class Translation {
    public static final int TRANSLATION_UNIT = 1;
    private int deltaX;
    private int deltaY;
    public Translation(int deltaX, int deltaY) {
        super();
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }
    public final int getDeltaX() {
        return deltaX;
    }
    public final int getDeltaY() {
        return deltaY;
    }
    
    
    public final static Translation getTranslation(Motion motionOrder,Orientation currentOrientation){
        
        int translationValue = motionOrder==Motion.A ? TRANSLATION_UNIT : 0;
        
        Translation translation=null;
        
        switch (currentOrientation) {
        case N :
            translation =  new Translation(0,translationValue);
            break;
        case W :
            translation =  new Translation(-translationValue,0);
            break;
        case S :
            translation =  new Translation(0,-translationValue);
            break;
        case E :
            translation =  new Translation(translationValue,0);
            break;
         //no default value reachable    
        }
        
        return translation;
    }
    

}
