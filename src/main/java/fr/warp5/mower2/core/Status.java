package fr.warp5.mower2.core;

public class Status {
    private Position position;
    private Orientation orientation;
    
    public Status(Position position, Orientation orientation) {
        super();
        this.position = position;
        this.orientation = orientation;
    }

    public final Position getPosition() {
        return position;
    }

    public final Orientation getOrientation() {
        return orientation;
    }
    
}
