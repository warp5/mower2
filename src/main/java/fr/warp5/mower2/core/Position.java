package fr.warp5.mower2.core;

public class Position {
    private int x;
    private int y;
    public Position(int x,int y) {
        super();
        this.x = x;
        this.y= y ;
    }
    public final int getX() {
        return x;
    }
    public final int getY() {
        return y;
    }
    
    
    public Position applyTranslation(Translation translation){
        return new Position(x+translation.getDeltaX(), y+translation.getDeltaY());
    }
    
    
    public String toString(){
        return "("+x+","+y+")";
    }

    
}
