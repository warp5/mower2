package fr.warp5.mower2.core;

import java.util.List;
import java.util.logging.Logger;

import fr.warp5.mower2.engine.IMotionEngine;


public class Mower {
    
    private static final Logger LOGGER = Logger.getLogger(Mower.class.getName());
    
    private Status currentStatus;
    
    
    private IMotionEngine motionEngine;
   
    
    public Mower(Status initialStatus,IMotionEngine motionEngine) {
        super();
        this.currentStatus = initialStatus;
        this.motionEngine = motionEngine;
    }

    public Status move(Motion motion){
        currentStatus = motionEngine.move(currentStatus,motion);
        return currentStatus;
    }
    
    
    public Status move(List<Motion> motions){
        Status status = currentStatus;
        for (Motion motion : motions){
            status = move(motion);
        }
        LOGGER.info("Mower stop at position "+status.getPosition()+ " with orientation "+status.getOrientation().name());
        return status;
    }
   
    

}
