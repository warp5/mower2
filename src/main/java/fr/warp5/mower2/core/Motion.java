package fr.warp5.mower2.core;

public enum Motion {
    D(Rotation.RIGHT),G(Rotation.LEFT),A(null);
    
    private Rotation rotation;

    private Motion(Rotation rotation) {
        this.rotation = rotation;
    }

    public final Rotation getRotation() {
        return rotation;
    }
    
    
}
