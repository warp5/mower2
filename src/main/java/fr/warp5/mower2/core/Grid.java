package fr.warp5.mower2.core;


public class Grid {
    
    final private int originX=0;
    final private int originY=0;
    private int sizeX;
    private int sizeY;
    
      
    public Grid(int sizeX, int sizeY) {
        super();
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }
    /**
     * used only for testing configuration reader
     * @return
     */
    public final int getSizeX() {
        return sizeX;
    }
    /**
     * used only for testing configuration reader
     * @return
     */
    public final int getSizeY() {
        return sizeY;
    }
    
    public boolean includes(Position position){
        return position.getX()>= originX && position.getX()<= sizeX
                && position.getY()>= originY && position.getY()<= sizeY;
    }
    
    

}
