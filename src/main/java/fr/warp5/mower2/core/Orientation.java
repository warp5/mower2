package fr.warp5.mower2.core;


public enum Orientation {
    N,E,S,W;
    
    public Orientation applyRotation(Rotation rotation){
        Orientation result = this;
        if (rotation!=null){
            result = rotation==Rotation.RIGHT ? right90degrees() : left90degrees();
        }
        return result;
     }

    public Orientation left90degrees() {
        
        return this == N ? W : this == W  ? S : this == S  ? E : N;
   }
/*
    public Orientation right90degrees() {
        return this == N ? E : this == E  ? S : this == S? W : N;
    }*/

   /* public Orientation left90degrees() {
        return Orientation.values()[(this.ordinal()-1)%4];
   }*/

    public Orientation right90degrees() {
        return Orientation.values()[(this.ordinal()+1)%4];
    }

}
