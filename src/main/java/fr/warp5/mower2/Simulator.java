package fr.warp5.mower2;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.warp5.mower2.config.Configuration;
import fr.warp5.mower2.config.FileConfigurationReaderImpl;
import fr.warp5.mower2.core.Status;
import fr.warp5.mower2.engine.IMotionEngine;
import fr.warp5.mower2.engine.MotionEngineImpl;

public class Simulator {
    
    private static final Logger LOGGER = Logger.getLogger(Simulator.class.getName());

    private static final int FAILURE = -1;
    private static final int SUCCESS = 0;
    private IMotionEngine engine;
    private List<Scenario> scenarios;
    
    
    /**
     * main program
     * @param configuration
     */
    public Simulator(Configuration configuration) {
        super();
        this.engine = new MotionEngineImpl(configuration.getGrid());
        this.scenarios = configuration.getScenarios();
    }
    /**
     * 
     * @return List<Status> (for testing)
     */
    public List<Status> run(){
        List<Status> report = new ArrayList<Status>(scenarios.size());
        for (Scenario scenario : scenarios){
            report.add(new Simulation(engine, scenario).run());
        }
        return report;
    }    
    
    
    public static void main(String[] args) {
        try {
            Configuration config = new FileConfigurationReaderImpl(new InputStreamReader( Simulator.class.getResourceAsStream(Configuration.MOWER2_CONFIG_FILE_NAME))).getConfiguration();
            new Simulator(config).run();
            System.exit(SUCCESS);
        } catch (Exception e) { // i.e : InputMismatchException
           LOGGER.log(Level.SEVERE, "Fatal exception "+e.getMessage());
           System.exit(FAILURE);
        }
    }
    
}
