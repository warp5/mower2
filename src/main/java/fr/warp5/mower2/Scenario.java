package fr.warp5.mower2;

import java.util.List;

import fr.warp5.mower2.core.Motion;
import fr.warp5.mower2.core.Status;

public class Scenario {
    private Status initialStatus;
    private List<Motion> commands;
    public Scenario(Status initialStatus, List<Motion> commands) {
        super();
        this.initialStatus = initialStatus;
        this.commands = commands;
    }
    public final Status getInitialStatus() {
        return initialStatus;
    }
    public final List<Motion> getCommands() {
        return commands;
    }
    

}
