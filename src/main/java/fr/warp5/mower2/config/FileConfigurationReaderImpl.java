package fr.warp5.mower2.config;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fr.warp5.mower2.Scenario;
import fr.warp5.mower2.core.Grid;
import fr.warp5.mower2.core.Motion;
import fr.warp5.mower2.core.Orientation;
import fr.warp5.mower2.core.Position;
import fr.warp5.mower2.core.Status;

public class FileConfigurationReaderImpl implements IConfigurationReader {
 
    private Reader reader;
    private int lineNumber=0;
    public FileConfigurationReaderImpl(Reader reader) {
       this.reader = reader;
    }

    
    public Configuration getConfiguration() {
        Grid grid=null;
        List<Scenario> scenarios = new ArrayList<Scenario>();

        Status currentStatus = null;
        
        //Try-with-resources (to close scanner if InputMismatchException caused by
        //incorrect format in config file)
        try (Scanner scanner = new Scanner(reader)) {
            while (scanner.hasNextLine()) {
                if (isGridConfigLine()){                
                    grid = new Grid(scanner.nextInt(),scanner.nextInt());
                } else if (isMowerPositionLine()) {
                    Position position = new Position(scanner.nextInt(),scanner.nextInt());
                    Orientation orientation = Orientation.valueOf(scanner.next()) ;
                    currentStatus = new Status(position,orientation);
                  } else if (isMotionsPositionLine()) {
                    List<Motion> motions = lineConfigMotionsToObject(scanner.next());
                    Scenario s = new Scenario(currentStatus,motions);
                    scenarios.add(s);
                } 
                
                ++lineNumber;
            }            
        }
        return new Configuration(grid,scenarios);
    }
    private boolean linePair() {
        return lineNumber%2==0;
    }   
        
    private boolean isGridConfigLine(){
        return lineNumber==0;
    }
    private boolean isMowerPositionLine(){
        return !linePair();
    }    
    private boolean isMotionsPositionLine(){
        return linePair() && !isGridConfigLine() ; 

    }
    private List<Motion> lineConfigMotionsToObject(String line){
        List<Motion> motions = new ArrayList<Motion>(line.length());
        char[] motionsChar = line.toCharArray();
        for (char motionChar : motionsChar){
            motions.add(Motion.valueOf(""+motionChar));
        }
        
        return motions;
    }
}
