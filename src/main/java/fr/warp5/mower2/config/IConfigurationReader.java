package fr.warp5.mower2.config;

public interface IConfigurationReader {
    Configuration getConfiguration();
}
