package fr.warp5.mower2.config;

import java.util.List;

import fr.warp5.mower2.Scenario;
import fr.warp5.mower2.core.Grid;

public class Configuration {
    public static final String MOWER2_CONFIG_FILE_NAME = "mower2.config";
    private Grid grid;
    private List<Scenario> scenarios;
    public Configuration(Grid grid, List<Scenario> scenarios) {
        super();
        this.grid = grid;
        this.scenarios = scenarios;
    }
    public final Grid getGrid() {
        return grid;
    }
    public final List<Scenario> getScenarios() {
        return scenarios;
    }
    

}
