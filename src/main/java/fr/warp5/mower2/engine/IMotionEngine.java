package fr.warp5.mower2.engine;

import fr.warp5.mower2.core.Motion;
import fr.warp5.mower2.core.Status;

public interface IMotionEngine {

    Status move(Status currentStatus, Motion direction);

}
