package fr.warp5.mower2.engine;

import fr.warp5.mower2.core.Grid;
import fr.warp5.mower2.core.Motion;
import fr.warp5.mower2.core.Orientation;
import fr.warp5.mower2.core.Position;
import fr.warp5.mower2.core.Status;
import fr.warp5.mower2.core.Translation;

public class MotionEngineImpl implements IMotionEngine {
    
    
    private Grid grid;

    public MotionEngineImpl(Grid grid) {
        super();
        this.grid = grid;
    }



    @Override
    public Status move(Status currentStatus, Motion motionOrder) {
       
        Orientation orientationOrderOncurrent = currentStatus.getOrientation().applyRotation(motionOrder.getRotation());
        Translation translationOrder = Translation.getTranslation(motionOrder, orientationOrderOncurrent);
        Position positionOrder = currentStatus.getPosition().applyTranslation(translationOrder);

        Status resultStatus = grid.includes(positionOrder) ? new Status(positionOrder,orientationOrderOncurrent) : currentStatus;

        return resultStatus ;
    }

}
