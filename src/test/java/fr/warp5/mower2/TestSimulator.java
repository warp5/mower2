package fr.warp5.mower2;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.FileNotFoundException;
import java.util.List;

import org.junit.Test;

import fr.warp5.mower2.config.Configuration;
import fr.warp5.mower2.core.Grid;
import fr.warp5.mower2.core.Orientation;
import fr.warp5.mower2.core.Status;

public class TestSimulator {
    @Test
    public void testSimulator() throws FileNotFoundException{
        
        Configuration configuration = new Configuration(new Grid(ScenarioDataSet.GRID_SIDE,ScenarioDataSet.GRID_SIDE), ScenarioDataSet.targetScenarios());
        Simulator simulator = new Simulator(configuration);
        List<Status> report = simulator.run();
        
        assertThat(report).hasSize(2);
        
        assertThat(report.get(0).getOrientation()).isEqualTo(Orientation.N);
        assertThat(report.get(0).getPosition().getX()).isEqualTo(1);
        assertThat(report.get(0).getPosition().getY()).isEqualTo(3);
        
        assertThat(report.get(1).getOrientation()).isEqualTo(Orientation.E);
        assertThat(report.get(1).getPosition().getX()).isEqualTo(5);
        assertThat(report.get(1).getPosition().getY()).isEqualTo(1);
        
    }
}
