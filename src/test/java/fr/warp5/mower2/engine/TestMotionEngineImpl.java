package fr.warp5.mower2.engine;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import fr.warp5.mower2.core.Grid;
import fr.warp5.mower2.core.Motion;
import fr.warp5.mower2.core.Orientation;
import fr.warp5.mower2.core.Position;
import fr.warp5.mower2.core.Status;

public class TestMotionEngineImpl {
    
    @Test
    public void testMove(){
        
        Status initialStatus = new Status(new Position(1, 2), Orientation.N);
        
        MotionEngineImpl engine = new MotionEngineImpl(new Grid(5,5));
        Status firstMove =engine.move(initialStatus, Motion.A);
        assertThat(firstMove.getOrientation()).isEqualTo(Orientation.N);
        assertThat(firstMove.getPosition().getX()).isEqualTo(1);
        assertThat(firstMove.getPosition().getY()).isEqualTo(3);

        
        Status secondMove =engine.move(firstMove, Motion.D);
        assertThat(secondMove.getOrientation()).isEqualTo(Orientation.E);
        assertThat(secondMove.getPosition().getX()).isEqualTo(1);
        assertThat(secondMove.getPosition().getY()).isEqualTo(3);
    }

}
