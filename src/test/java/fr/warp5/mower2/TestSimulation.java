package fr.warp5.mower2;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import fr.warp5.mower2.core.Grid;
import fr.warp5.mower2.core.Orientation;
import fr.warp5.mower2.core.Status;
import fr.warp5.mower2.engine.MotionEngineImpl;

public class TestSimulation {
    
   
    @Test
    public void  testScenario1BorderGrid(){
        
        Scenario scenario = ScenarioDataSet.scenario1();
        
        Simulation simulation = new Simulation(new MotionEngineImpl(new Grid(ScenarioDataSet.GRID_SIDE,ScenarioDataSet.GRID_SIDE)), scenario);
        Status finalStatus = simulation.run();

        assertThat(finalStatus.getOrientation()).isEqualTo(Orientation.N);
        assertThat(finalStatus.getPosition().getX()).isEqualTo(1);
        assertThat(finalStatus.getPosition().getY()).isEqualTo(3);
    }



    @Test
    public void  testScenario2BorderGrid(){
        Scenario scenario = ScenarioDataSet.scenario2();
        
        Simulation simulation = new Simulation(new MotionEngineImpl(new Grid(ScenarioDataSet.GRID_SIDE,ScenarioDataSet.GRID_SIDE)), scenario);
        Status finalStatus = simulation.run();

        assertThat(finalStatus.getOrientation()).isEqualTo(Orientation.E);
        assertThat(finalStatus.getPosition().getX()).isEqualTo(5);
        assertThat(finalStatus.getPosition().getY()).isEqualTo(1);
    }



}
