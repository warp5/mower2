package fr.warp5.mower2.config;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

import fr.warp5.mower2.ScenarioDataSet;
import fr.warp5.mower2.core.Motion;
import fr.warp5.mower2.core.Orientation;
public class TestFileConfigurationReader {
    
    
    @Test
    public void testParse() throws FileNotFoundException{
        FileConfigurationReaderImpl reader = new FileConfigurationReaderImpl(new FileReader("src/test/resources/mower2TargetConfig.config"));
        Configuration config = reader.getConfiguration();
        assertThat(config.getGrid().getSizeX()).isEqualTo(ScenarioDataSet.GRID_SIDE);
        assertThat(config.getGrid().getSizeY()).isEqualTo(ScenarioDataSet.GRID_SIDE);
        assertThat(config.getScenarios()).hasSize(2);
        assertThat(config.getScenarios().get(0).getCommands()).hasSize(9);
        assertThat(config.getScenarios().get(0).getCommands().get(0)).isEqualTo(Motion.G);
        assertThat(config.getScenarios().get(0).getCommands().get(8)).isEqualTo(Motion.A);
        assertThat(config.getScenarios().get(0).getInitialStatus().getPosition().getX()).isEqualTo(1);
        assertThat(config.getScenarios().get(0).getInitialStatus().getPosition().getY()).isEqualTo(2);
        assertThat(config.getScenarios().get(0).getInitialStatus().getOrientation()).isEqualTo(Orientation.N);
        assertThat(config.getScenarios().get(1).getCommands()).hasSize(10);
        assertThat(config.getScenarios().get(1).getCommands().get(0)).isEqualTo(Motion.A);
        assertThat(config.getScenarios().get(1).getCommands().get(9)).isEqualTo(Motion.A);
        assertThat(config.getScenarios().get(1).getInitialStatus().getPosition().getX()).isEqualTo(3);
        assertThat(config.getScenarios().get(1).getInitialStatus().getPosition().getY()).isEqualTo(3);
        assertThat(config.getScenarios().get(1).getInitialStatus().getOrientation()).isEqualTo(Orientation.E);       
    }

}
