package fr.warp5.mower2.core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.warp5.mower2.core.Grid;
import fr.warp5.mower2.core.Position;

public class TestGrid {
    
    @Test
    public void testIncludes(){
        Grid law = new Grid(5,5);
        assertTrue(law.includes(new Position(0,0)));
        assertTrue(law.includes(new Position(5,5)));
        assertTrue(law.includes(new Position(0,5)));
        assertTrue(law.includes(new Position(5,0)));
        assertTrue(law.includes(new Position(1,3)));
        assertFalse(law.includes(new Position(0,6)));
        assertFalse(law.includes(new Position(-1,2)));
        assertFalse(law.includes(new Position(-1,6)));
    }

}
