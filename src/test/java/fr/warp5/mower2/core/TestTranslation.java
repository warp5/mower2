package fr.warp5.mower2.core;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class TestTranslation {
    @Test
    public void testTranslation(){
        
        // A
        
        Translation goToNorth = Translation.getTranslation(Motion.A, Orientation.N);
        assertThat(goToNorth.getDeltaX()).isEqualTo(0);
        assertThat(goToNorth.getDeltaY()).isEqualTo(Translation.TRANSLATION_UNIT);

        Translation goToWest = Translation.getTranslation(Motion.A, Orientation.W);
        assertThat(goToWest.getDeltaX()).isEqualTo(-Translation.TRANSLATION_UNIT);
        assertThat(goToWest.getDeltaY()).isEqualTo(0);
        
        Translation goToSouth = Translation.getTranslation(Motion.A, Orientation.S);
        assertThat(goToSouth.getDeltaX()).isEqualTo(0);
        assertThat(goToSouth.getDeltaY()).isEqualTo(-Translation.TRANSLATION_UNIT);
        
        Translation goToEast = Translation.getTranslation(Motion.A, Orientation.E);
        assertThat(goToEast.getDeltaX()).isEqualTo(Translation.TRANSLATION_UNIT);
        assertThat(goToEast.getDeltaY()).isEqualTo(0);

        //D & G
        
        Translation rotateN_D = Translation.getTranslation(Motion.D, Orientation.N);
        assertThat(rotateN_D.getDeltaX()).isEqualTo(0);
        assertThat(rotateN_D.getDeltaY()).isEqualTo(0);

        
        Translation rotateS_D = Translation.getTranslation(Motion.D, Orientation.S);
        assertThat(rotateS_D.getDeltaX()).isEqualTo(0);
        assertThat(rotateS_D.getDeltaY()).isEqualTo(0);

        Translation rotateS_G = Translation.getTranslation(Motion.G, Orientation.S);
        assertThat(rotateS_G.getDeltaX()).isEqualTo(0);
        assertThat(rotateS_G.getDeltaY()).isEqualTo(0);
        
        //null motion
        Translation nullMotion = Translation.getTranslation(null, Orientation.S);
        assertThat(nullMotion.getDeltaX()).isEqualTo(0);
        assertThat(nullMotion.getDeltaY()).isEqualTo(0);
        
        
    }

}
