package fr.warp5.mower2.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.warp5.mower2.core.Orientation;
import fr.warp5.mower2.core.Rotation;

public class TestOrientation {
    
    @Test
    public void testRight(){
        assertEquals(Orientation.E,Orientation.N.right90degrees());
        assertEquals(Orientation.S,Orientation.E.right90degrees());
        assertEquals(Orientation.W,Orientation.S.right90degrees());
        assertEquals(Orientation.N,Orientation.W.right90degrees());
    }
    
    @Test
    public void testLeft(){
        assertEquals(Orientation.E,Orientation.S.left90degrees());
        assertEquals(Orientation.S,Orientation.W.left90degrees());
        assertEquals(Orientation.W,Orientation.N.left90degrees());
        assertEquals(Orientation.N,Orientation.E.left90degrees());
    }
    
    @Test
    public void testApplyRotation(){
        assertEquals(Orientation.E,Orientation.E.applyRotation(null));
        assertEquals(Orientation.E,Orientation.N.applyRotation(Rotation.RIGHT));
        assertEquals(Orientation.S,Orientation.E.applyRotation(Rotation.RIGHT));
        assertEquals(Orientation.W,Orientation.S.applyRotation(Rotation.RIGHT));
        assertEquals(Orientation.N,Orientation.W.applyRotation(Rotation.RIGHT));  
        assertEquals(Orientation.E,Orientation.S.applyRotation(Rotation.LEFT));
        assertEquals(Orientation.S,Orientation.W.applyRotation(Rotation.LEFT));
        assertEquals(Orientation.W,Orientation.N.applyRotation(Rotation.LEFT));
        assertEquals(Orientation.N,Orientation.E.applyRotation(Rotation.LEFT));
    }

}
