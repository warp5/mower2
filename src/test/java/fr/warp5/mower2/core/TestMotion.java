package fr.warp5.mower2.core;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import fr.warp5.mower2.core.Motion;
import fr.warp5.mower2.core.Rotation;

public class TestMotion {
    @Test
    public void testGetRotation(){
        assertThat(Motion.A.getRotation()).isNull();
        assertThat(Motion.G.getRotation()).isEqualTo(Rotation.LEFT);
        assertThat(Motion.D.getRotation()).isEqualTo(Rotation.RIGHT);
        
    }
}
