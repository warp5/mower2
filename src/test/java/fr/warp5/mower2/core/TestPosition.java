package fr.warp5.mower2.core;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import fr.warp5.mower2.core.Translation;

public class TestPosition {
    @Test
    public void testApplyTranslation(){
        Position departure = new Position(1, 1);
        Position arrival = departure.applyTranslation(new Translation(3,3));
        assertThat(arrival.getX()).isEqualTo(4);
        assertThat(arrival.getY()).isEqualTo(4);
    }
}
