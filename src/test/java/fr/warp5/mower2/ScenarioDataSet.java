package fr.warp5.mower2;

import java.util.ArrayList;
import java.util.List;

import fr.warp5.mower2.core.Motion;
import fr.warp5.mower2.core.Orientation;
import fr.warp5.mower2.core.Position;
import fr.warp5.mower2.core.Status;

public class ScenarioDataSet {
    
    
    public static final int GRID_SIDE = 5;
    
    public static List<Scenario> targetScenarios(){
        List<Scenario> scenarios = new ArrayList<Scenario>(2);
        scenarios.add(scenario1());
        scenarios.add(scenario2());
        return scenarios;
    }
    
    
    public static Scenario scenario1() {
        List<Motion> commands = new ArrayList<Motion>();
        commands.add(Motion.G);
        commands.add(Motion.A);
        commands.add(Motion.G);
        commands.add(Motion.A);
        commands.add(Motion.G);
        commands.add(Motion.A);
        commands.add(Motion.G);
        commands.add(Motion.A);
        commands.add(Motion.A);

        Scenario scenario = new Scenario(new Status(new Position(1, 2), Orientation.N), commands);
        return scenario;
    }
    public static Scenario scenario2() {
        List<Motion> commands = new ArrayList<Motion>();
        commands.add(Motion.A);
        commands.add(Motion.A);
        commands.add(Motion.D);
        commands.add(Motion.A);
        commands.add(Motion.A);
        commands.add(Motion.D);
        commands.add(Motion.A);
        commands.add(Motion.D);
        commands.add(Motion.D);
        commands.add(Motion.A);

        Scenario scenario = new Scenario(new Status(new Position(3, 3), Orientation.E), commands);
        return scenario;
    }
}
